local secrets = import '../secrets.jsonnet';
local k = import 'k.libsonnet';

k.core.v1.secret.new('bitcoind', secrets.dev.bitcoind) + k.core.v1.secret.metadata.withNamespace('bitcoin')
