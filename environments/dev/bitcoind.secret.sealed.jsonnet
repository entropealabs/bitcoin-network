{
  "kind": "SealedSecret",
  "apiVersion": "bitnami.com/v1alpha1",
  "metadata": {
    "name": "bitcoind",
    "namespace": "bitcoin",
    "creationTimestamp": null,
    "annotations": {
      "sealedsecrets.bitnami.com/namespace-wide": "true"
    }
  },
  "spec": {
    "template": {
      "metadata": {
        "name": "bitcoind",
        "namespace": "bitcoin",
        "creationTimestamp": null,
        "annotations": {
          "sealedsecrets.bitnami.com/namespace-wide": "true"
        }
      },
      "type": "Opaque",
      "data": null
    },
    "encryptedData": {
      "rpcauth": "AgBcMNaSiLhS3npMUhUcirC5VC3VaBDfKiVcSFZbvTygJ8ryeyOjzxwPhUDrwjoi8241W8tH41TugrCLpNPHX4mzBx6c51Vss6objrq4dMNfC4GSo5jgEcdrdW7yfYZ7tKuCdItokddZI2vh3FP69oon7sKtMLSQemB1fwKkIAZrkEJYVNAxnX4ByUz6W1WLWs4fn3fSF6fCTc8ptiqsnGEx1JJbrSI39DFjDCilr2cpfr2BLSRD27esTn2qPO0hI7m/CX5aRRbzOIYdSuVW/9LHaWZMocg7E9Ayzn/SZNCu27BPPfPDKSBX82uBjdwLU98aKD7NR3O30wEu3ymlpWS0XsT/kgnKsiWOZe5afFWOWImuVucFW3PoCuBLI/y4e57zxfJHl8Jf9xeAsNJ987QDOw4i1InXEkhLvmFhayH0+pZ6HgfhVJRUB3CM2FYDaT3LG2DiAqH0vaEaItxHI9BVuoU2ASadH4XQ3WAeA38wbEKZPnxckYbIPWvzrP91qZCeZq73183Zaw0zI+cvpQE0HpLLpoCntKcc25+4c81R7TggzlSEDo41+N5SxkosmajrJhMaithbOVjThqBIC8ABoyTDywzsluuzAa3p5GhQXECmxk9GQIYC6uN+CRB9We1fiZMrMInhZQXh0NAV5PlDntUuLUyXhB3VoJ8e9fHnSlycyZUBZrPwnxGvAzE/wKW77t5dT0zay4mRR0SBdTaP8zD41zD7NzRsJF/vuWbJaZD2z6F1DQQHDTyAUapgjyyS2obFX1/V/eZZwBmgyJr6q4q7W+b1W+lCMkKd+IlF8+xaViAR153ucaZiZqmaA2IF1bV6iHVKX0A="
    }
  }
}
