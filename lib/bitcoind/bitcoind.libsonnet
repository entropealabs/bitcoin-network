local k = import 'k.libsonnet';

(import './config.libsonnet') +
{
  local container = k.core.v1.container,
  local deployment = k.apps.v1.deployment,
  local pvc = k.core.v1.persistentVolumeClaim,
  local service = k.core.v1.service,
  local cm = k.core.v1.configMap,
  local c = $._config.bitcoind,
  local s = $._config.satip,
  local port = k.core.v1.servicePort.new(c.port, c.port) + k.core.v1.servicePort.withNodePort(c.node_port),
  local c_port = k.core.v1.containerPort.new(c.port),
  local z_block_port = k.core.v1.containerPort.new(c.zmq_block_port),
  local z_tx_port = k.core.v1.containerPort.new(c.zmq_tx_port),
  local configmap = cm.new(s.name, s.config_map.data),

  deployment: deployment.new(
                name=c.name,
                containers=[
                  container.new(name=s.name, image=$._images.bitcoind.bitcoind)
                  + container.withCommand(s.command)
                  + container.withImagePullPolicy(c.image_pull_policy)
                  + container.withArgs(s.args),

                  container.new(name=c.name, image=$._images.bitcoind.bitcoind)
                  + container.withCommand(c.command)
                  + container.withEnvMixin([k.core.v1.envVar.fromSecretRef('RPC_AUTH', c.name, 'rpcauth')])
                  + container.withImagePullPolicy(c.image_pull_policy)
                  + container.withPorts([c_port, z_block_port, z_tx_port])
                  + container.withArgs(c.args),
                ]
              )
              + deployment.pvcVolumeMount(name=c.pvc_name, path=c.data_path)
              + deployment.configMapVolumeMount(configmap, s.config_map.path, k.core.v1.volumeMount.withSubPath(s.config_map.sub_path))
              + deployment.spec.template.spec.withTerminationGracePeriodSeconds(60),

  configmap: configmap,
  service: service.new(c.name, { name: c.name }, port)
           + service.spec.withType(c.service_type),
  pvc: pvc.new(name=c.pvc_name)
       + pvc.spec.withAccessModes(c.pvc.access_mode)
       + pvc.spec.resources.withRequests({ storage: c.pvc.storage })
       + pvc.spec.withStorageClassName(c.pvc.class_name),
}
