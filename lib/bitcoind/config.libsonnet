{
  _config+:: {
    bitcoind: {
      pvc: {
        access_mode: ['ReadWriteOnce'],
        storage: '1000Gi',
        class_name: 'microk8s-hostpath',
      },
      service_type: 'NodePort',
      name: 'bitcoind',
      port: 8332,
      image_pull_policy: 'IfNotPresent',
      node_port: 32332,
      zmq_block_port: 28332,
      zmq_tx_port: 28333,
      data_path: '/data/bitcoin',
      pvc_name: 'bitcoin-data',
      command: ['bitcoind'],
      args: [
        '-datadir=' + self.data_path,
        '-server=1',
        '-txindex=1',
        '-maxconnections=10',
        '-blocksonly=1',
        '-blockfilterindex=1',
        '-rpcauth=$(RPC_AUTH)',
        '-rpcallowip=0.0.0.0/0',
        '-rpcbind=0.0.0.0',
        '-rpcport=8332',
        '-zmqpubrawblock=tcp://127.0.0.1:28332',
        '-zmqpubrawtx=tcp://127.0.0.1:28333',
        '-debug=udpnet',
        '-debug=udpmulticast',
        '-udpmulticastloginterval=60',
        '-udpmulticast=lo,239.0.0.2:4434,127.0.0.1,1,blocksat-sat-ip',
      ],
    },
    satip: {
      name: 'sat-ip',
      ip: '192.168.0.182',
      command: ['blocksat-cli'],
      args: [
        'sat-ip',
        '-a' + self.ip,
        '--log-scrolling',
      ],
      config_map: {
        data: {
          'config.json': std.manifestJsonEx(import './blocksat_config.json', ''),
        },
        sub_path: 'config.json',
        path: '/root/.blocksat/' + self.sub_path,
      },

    },
  },
  _images+:: {
    bitcoind: {
      bitcoind: 'registry.gitlab.com/entropealabs/satellite/blocksat-host:latest',
    },
  },
}
